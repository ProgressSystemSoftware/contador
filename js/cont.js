var n;
var n2 = 0;
contador_m = 0;
contador_mil = 0;
var centesimas = 0;
var segundos = 0;
var minutos = 0;
var horas = 0;
storage = window.localStorage;

angular.module("app", [])
    .controller("Nuevo", function($scope, $http) {
		if (storage.getItem("contador_s") === "undefined" || storage.getItem("contador_s") === "NaN" || storage.getItem("contador_s") == null) {
            n = 1;
            $scope.number = 1;
            storage.setItem("contador_s", n);
            console.log("primera vez")
        } else {
            savedValue = parseInt(storage.getItem("contador_s"));
            n = savedValue;
            $scope.number = savedValue;
            console.log("en delante vez")
        }

		texto.innerHTML = "Se han registrado " + n2 + " en " + contador_m + " minutos"
		contador_s = n + numeroAleatorio(0, 1);
		s.innerHTML = "000" + contador_s;
		ss.innerHTML = "000" + contador_s;
		$scope.inicio = function() {
			control = setInterval(cronometro, document.getElementById("tiempo").value);
			control2 = setInterval(cronometro2, 10);
		}

		$scope.parar = function() {
			clearInterval(control);
			clearInterval(control2);
		}

		$scope.reset = function() {
			contador_s = 0;
			storage.setItem("contador_s", contador_s);
			n2 = 0;
			contador_m = 0;
			contador_mil = 0;
			centesimas = 0;
			segundos = 0;
			minutos = 0;
			horas = 0;
			s.innerHTML = "000" + contador_s;
			ss.innerHTML = "000" + contador_s;
		}

		function cronometro() {
			s = document.getElementById("s");
			ss = document.getElementById("ss");
			contador_mil += 5;
			if (contador_mil >= 1000) {
				contador_mil = 0;
				if (contador_s >= 60) {
					contador_m++;
					storage.setItem("contador_m", contador_m);
				}
				document.getElementById('conteo').play();
				n = 0;

				while(n <= contador_s){
					n = n + 100;
					if (contador_s == n) {
						document.getElementById('myAudio').play();
					}
				}

				$scope.insertdata;
				contador_s++;
				n2++;
				storage.setItem("contador_s", contador_s);
				s.innerHTML =  "000" + (contador_s);
				ss.innerHTML = "000" + (contador_s);
			}
		}

		function cronometro2 () {
			if (centesimas < 99) {
				centesimas++;
				if (centesimas < 10) { centesimas = "0"+centesimas }
			}
			if (centesimas == 99) {
				centesimas = -1;
			}
			if (centesimas == 0) {
				segundos ++;
				if (segundos < 10) { segundos = "0"+segundos }
				texto.innerHTML = "Se han registrado " + n2 + " tiempo " + horas + ":" 
					+ minutos + ":" + segundos + ":" + centesimas;
			}
			if (segundos == 59) {
				segundos = -1;
			}
			if ( (centesimas == 0)&&(segundos == 0) ) {
				minutos++;
				if (minutos < 10) { minutos = "0"+minutos }
			}
			if (minutos == 59) {
				minutos = -1;
			}
			if ( (centesimas == 0)&&(segundos == 0)&&(minutos == 0) ) {
				horas ++;
				if (horas < 10) { horas = "0"+horas }
			}
		}
		
		var name = document.getElementById('name');
		var pw = document.getElementById('pw');

		$scope.check = function () {
			if(userName.value == "user" && userPw.value == "pass") {
				$(".objetos").css("display" , "block");
				$("#login-form").css("display" , "none");
			}else {
				alert('ERROR');
			}
		}
});

function numeroAleatorio(min, max) {
	return Math.round(Math.random() * (max - min) + min);
}
